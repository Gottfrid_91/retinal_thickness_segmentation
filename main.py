import os
from image import Image
import glob
import pandas as pd
from tqdm import tqdm
if __name__ == "__main__":
    data_path = "/home/olle/PycharmProjects/retinal_layer_segmentation/data/all_images"
    cvs_path = "/home/olle/PycharmProjects/retinal_layer_segmentation/data/file_names_complete"

    # remaining low performing examples
    # 116
    # 541
    # 273 -  no reason / fibrosis
    # 30 - very dark
    # 44 - posteror hylaoid
    # 78 - noisy
    # 84
    # 464 - fibrosis
    # 541 - should impute rpe layer
    # 561 should impute rpe layer
    # 642 - impute rpe
    # 701 ?
    # 757 ?
    # 928
    # 847
    # 1017 impute rpe
    # 1073 impute rpe
    # 1252

    oct_paths = glob.glob(data_path + "/*.png")
    test_ids = pd.read_csv(os.path.join(cvs_path, "test_new_old_mapping.csv")).new_id.tolist()
    for oct_path in tqdm(oct_paths):
        if int(oct_path.split("/")[-1].replace(".png", "")) in test_ids:
            #if "1101.png" == oct_path.split("/")[-1]:
            #try:
            print(oct_path)
            image = Image(oct_path)
            image.preprocess_image()
            #except:
                #print(oct_path, "did not work")
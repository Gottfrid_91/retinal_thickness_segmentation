# get camera effect mask
camera_effect_mask = self.camera_effect(grey)

# extend camera effect boolean mask to exclude false gradients
extended_mask = self.extend_boolean_mask(camera_effect_mask, pad = 10)
'''
# create maks for halves
u_mask, l_mask = self.divide_masks(extended_mask)

# mirrored_image = self.paste_mirror_image(filtered_image, camera_effect_mask)

# remove false gradient
filtered_image[u_mask == 1] = np.quantile(filtered_image[filtered_image != 0], 0.25)
# remove false gradient
filtered_image[l_mask == 1] = np.quantile(filtered_image[filtered_image != 0], 0.01)
'''

# check if gradient map has two x ranging blobs
# extract blobs from clusters
label_post = label(gradient_map)
regions_post = regionprops(label_post)

# get range of remaining blobs
ranges = []
for region_post in regions_post:
    left, right = region_post[0].bbox[1], region_post[0].bbox[3]
    # get range of blob
    range = right - left
    if range > 400:
        ranges.append(range)




        # if we have less than 1 x spanning blob
        if len(ranges) < 2:



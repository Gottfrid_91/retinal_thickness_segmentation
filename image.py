import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import os
from scipy.ndimage import median_filter
from skimage import filters
from skimage.morphology import closing
from skimage.morphology import square
from skimage.measure import label, regionprops
from sklearn.neighbors import KernelDensity
from scipy.signal import find_peaks, peak_prominences, convolve2d
from scipy.stats import norm
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
'''
pre-processing as outlined in https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3408910/
any parameter value not mentioned specifically is set to default values 
'''

def plot_layer(layer):
    plt.plot(layer);
    plt.ylim([496, 0])
    plt.show()

class Image:
    def __init__(self, path):
        self.image_path = path
        self.image_name = path.split("/")[-1]
        self.image = cv2.imread(path)
        self.processed_image = None

    @staticmethod
    def signaltonoise(a, axis=0, ddof=0):
        a = np.asanyarray(a)
        m = a.mean(axis)
        sd = a.std(axis = axis, ddof = ddof)
        return np.where(sd == 0, 0, m / sd)

    @staticmethod
    def camera_effect(img):
        # annotate camera artifact
        if len(img.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        ret, mask = cv2.threshold(img, 0, 1, cv2.THRESH_BINARY)
        kernel = np.ones((20, 20), np.uint8)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        return ~mask // 255

    @staticmethod
    def set_bottom_layer(mask):
        right_bottom_layer = []
        left_bottom_layer = []
        for i in range(1, mask.shape[1]-1, 1):
            bef = np.where(mask[:, i - 1])
            current = np.where(mask[:, i])
            after = np.where(mask[:, i + 1])

            if bef[0].size == 0:
                bef = 0
            if current[0].size == 0:
                current = 0
            if after[0].size == 0:
                after = 0
            idx = np.max(current)
            if idx == 0:
                right_bottom_layer.append(np.nan)
            else:
                right_bottom_layer.append(idx)

        if not np.sum(np.isnan(right_bottom_layer)) == len(right_bottom_layer):
            # remove outlier low border pixels
            nan_bool = np.abs(np.diff(right_bottom_layer)) > 5

            right_bottom_layer_array = np.array(right_bottom_layer).astype(np.float)
            right_bottom_layer_array[:-1][nan_bool] = np.nan

            x = np.arange(0, right_bottom_layer_array.shape[0])
            y = right_bottom_layer_array

            nan_bool = np.isnan(y)

            x_fit = x[~nan_bool]
            y_fit = y[~nan_bool]

            f = interpolate.interp1d(x_fit, y_fit, kind = 1)

            # interpolate
            x_new = np.arange(np.min(x_fit), np.max(x_fit), 1)
            y_new = f(x_new).astype(int)

            for k, i in enumerate(range(np.min(x_fit), np.max(x_fit))):
                mask[y_new[k], i] = 1
        return mask

    def interpolate_missing_regions(self, layer, kind):
        # remove outlier low border pixels
        nan_bool = np.abs(np.diff(layer)) > 5

        layer_array = np.array(layer).astype(np.float)
        layer_array[:-1][nan_bool] = np.nan

        x = np.arange(0, layer_array.shape[0])
        y = layer_array

        nan_bool = np.isnan(y)

        x_fit = x[~nan_bool]
        y_fit = y[~nan_bool]

        if kind=="linear":
            f = interpolate.interp1d(x_fit, y_fit, kind = 1)
        if kind=="ius":
            f = InterpolatedUnivariateSpline(x_fit, y_fit, k=2)
        if kind == "rbf":
            f = Rbf(x_fit, y_fit, smooth=1)

        # interpolate
        x_new = np.arange(np.min(x_fit), np.max(x_fit), 1)
        y_new = f(x_new).astype(int)
        return y_new

    def keep_largest_blob(self, gradient_map):
        # extract blobs from clusters
        label_img = label(gradient_map)
        regions = regionprops(label_img)

        filter_size = np.max([region.bbox_area for region in regions])
        # remove all blobs smaller than 1000 pixels
        for region in regions:
            blob_label = region.label
            area = region.bbox_area

            if area < filter_size:
                gradient_map[label_img == blob_label] = 0
        return gradient_map

    def closing(self, map):
        # close closeby regions
        return closing(map, square(10)).astype(int)

    def blob_filtering(self, gradient_map, filter_size):
        # extract blobs from clusters
        label_img = label(gradient_map)
        regions = regionprops(label_img)

        blob_sizes = [region.bbox_area for region in regions]
        # remove all blobs smaller than 1000 pixels
        for region in regions:
            blob_label = region.label
            area = region.bbox_area

            if area < filter_size:
                gradient_map[label_img == blob_label] = 0
        # close closeby regions
        closed_gradient_map = closing(gradient_map, square(10)).astype(int)
        return closed_gradient_map

    def final_two_blob_filtering(self, gradient_map):
        # extract blobs from clusters
        label_img = label(gradient_map)
        regions = regionprops(label_img)

        second_largest = np.sort([region.bbox_area for region in regions])[-2:][0]
        # remove all blobs smaller than 1000 pixels
        for region in regions:
            blob_label = region.label
            area = region.bbox_area

            if area < second_largest:
                gradient_map[label_img == blob_label] = 0

        # close closeby regions
        closed_gradient_map = closing(gradient_map, square(10)).astype(int)
        return closed_gradient_map

    def filter_image(self, img):
        # set image to grey scale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # de-noise with gaussian filter, filter size set to default
        blur = cv2.GaussianBlur(gray, (3, 11), 25)
        kernel = np.ones((3, 19), np.float32) / (19 * 3)
        gray_filtered = cv2.filter2D(blur, -1, kernel)
        return gray_filtered.astype(float), gray

    def get_top_layer(self, mask, pad):
        layer = []
        for i in range(1, mask.shape[1]-1, 1):
            current = np.where(mask[:, i])
            if current[0].size == 0:
                current = 0
            idx = np.min(current).astype(int)
            if idx == 0:
                layer.append(np.nan)
            else:
                layer.append(idx)

        pos = np.where(np.abs(np.diff(layer)) >= 10)[0]
        layer = np.array(layer, dtype=np.float32)

        if pos != []:
            # if first and last discuntinuity are due to nans, remove
            if np.sum(np.isnan(layer[0: pos[0] - 1])) == pos[0] - 1:
                pos = np.delete(pos, 0)
            if np.sum(np.isnan(layer[pos[-1] -1:])) == layer.shape[0] - pos[-1] - 2:
                pos = np.delete(pos, -1)
        if pos != []:
            # if few value in beginning or end causes disc- then remove
            if np.sum(~np.isnan(layer[0: pos[0] - 1])) < 10:
                layer[0:pos[0] + 1] = np.nan
                pos = np.delete(pos, 0)
            if np.sum(~np.isnan(layer[pos[-1] - 1:])) < 10:
                layer[pos[-1] - 1:] = np.nan
                pos = np.delete(pos, -1)

            for i in range(0, pos.shape[0] // 2, 2):
                disc = pos[i:i + 2]
                layer[disc[0]:disc[1]] = np.nan

        if pad:
            for k, i in enumerate(reversed(np.isnan(layer))):
                if i:
                    layer[-k:-k+5] = layer[-k-1]
                    break
        return layer

    def get_median_layer(self, mask):
        layer = []
        for i in range(1, mask.shape[1]-1, 1):
            current = np.where(mask[:, i])
            if current[0].size == 0:
                current = 0
            idx = np.median(current).astype(int)
            if idx == 0:
                layer.append(np.nan)
            else:
                layer.append(idx)
        return layer
    def get_bottom_layer(self, mask, shift_log):
        layer = []
        h, w = mask.shape
        for i in range(1, mask.shape[1]-1, 1):
            current = np.where(mask[:, i])
            if current[0].size == 0:
                current = 0
            idx = np.max(current)
            if idx == 0:
                layer.append(np.nan)
            else:
                layer.append(idx)

        pos = np.where(np.abs(np.diff(layer)) >= 10)[0]
        layer = np.array(layer, dtype=np.float32)

        if pos != []:
            # if first and last discuntinuity are due to nans, remove
            if np.sum(np.isnan(layer[0: pos[0] - 1])) == pos[0] - 1:
                pos = np.delete(pos, 0)
            if np.sum(np.isnan(layer[pos[-1] -1:])) == layer.shape[0] - pos[-1] - 2:
                pos = np.delete(pos, -1)

            for i in range(0, pos.shape[0] // 2, 2):
                disc = pos[i:i + 2]
                layer[disc[0]:disc[1]] = np.nan

        if not np.isnan(layer).shape[0] == len(layer):
            # set middle line to nan for smooth interpolation
            layer[int(w/5):4*int(w/5)] = np.nan
            layer = self.interpolate_missing_regions(layer, kind="rbf")
        return layer

    @staticmethod
    def extend_boolean_mask(mask, pad):
        # pad boolean mask
        for col in range(mask.shape[1]):
            mask_col = mask[:, col]
            max_true = np.argmax(mask_col)
            min_true = np.argmin(mask_col)

            # increase region in y direction
            mask_col[min_true: min_true + pad] = 1
            mask_col[max_true - pad: max_true] = 1

        for row in range(mask.shape[0]):
            mask_row = mask[row, :]
            max_true = np.argmax(mask_col)
            min_true = np.argmin(mask_col)

            # increase region in y direction
            mask_col[min_true: min_true + pad] = 1
            mask_col[max_true - pad: max_true] = 1
        return mask

    def gradient_threshold(self, img):
        sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize = 5)
        threshold_value = filters.threshold_otsu(sobely)
        gradient_map = sobely > threshold_value
        return gradient_map, sobely

    def negative_gradient_threshold(self, sobely):
        threshold_value = filters.threshold_otsu(sobely) * -1
        gradient_map = sobely > threshold_value
        return gradient_map

    def threshold(self, img):
        threshold_value = filters.threshold_otsu(img)
        img_thresh = img > threshold_value
        return img_thresh

    def filtered_image_threshold(self, image):
        img = np.copy(image)
        rpe_roi = np.copy(img)
        threshold_value = filters.threshold_otsu(img)
        img_map = img > threshold_value
        top_values = np.argmin(~img_map, 0)
        for k, fp in enumerate(top_values):
            img[0:fp, k] = 0

        # extract max value from early column most often belonging to rpe
        delta = 50 # np.argmax(img[max_top_value:, 5])

        # set all values to zero for roi
        for k, fp in enumerate(top_values):
            #top_delta = max_top_value - fp
            rpe_roi[0:fp + delta, k] = 0
        return img, rpe_roi, img_map

    @staticmethod
    def closed_gradient_map_lower_roi(map, layer):
        shift_log = []
        for i in range(1, len(layer)):
            if not np.isnan(layer[i]):
                map[0:layer[i] + 40, i] = 0
                shift_log.append(layer[i] + 40)
            else:
                shift_log.append(np.nan)

        # set all pixels after layer to zero
        map[:, len(layer):] = 0
        return map, shift_log

    def display_result(self, record, save_path):
        w = 10
        h = 10
        fig = plt.figure(figsize = (w, h))
        columns = 2
        rows = 1
        for i in range(1, columns * rows + 1):
            img = record[i-1]
            fig.add_subplot(rows, columns, i)
            plt.imshow(img)
        plt.savefig(save_path)

    def paste_mirror_image(self, image, mask):
        image_mask = mask == 0
        for i in range(image.shape[1]):
            image_col = image[:, i]
            mask_col = mask[:, i]

            image_col_values = image_col[mask_col == 0]
            if image_col_values != []:
                for j in range(image_col.shape[0]):
                    if mask_col[j] == 1:
                        if j < (-image_col.shape[0] + 1):
                            image_col[j] = image_col_values[-1-j]
                        else:
                            image_col[j] = image_col_values[-1]

            image[:, i] = image_col
        return image

    def divide_masks(self, mask):
        u_mask = np.copy(mask)
        l_mask = np.copy(mask)

        u_mask[u_mask.shape[0] // 2:-1, :] = 0
        l_mask[0:l_mask.shape[0] // 2, :] = 0
        return u_mask, l_mask

    def remove_blobs_above_vitreous(self, img, layer):
        for i in range(layer.shape[0]):
            if not np.isnan(layer[i]):
                img[0:int(layer[i]) - 1, i] = 0
        return img

    def fit_line_through_blob(self, img):
        blobs = np.unique(img).tolist()
        line_dict = {}
        for blob in blobs:
            mask = img == blob
            blob_line = []
            for i in mask.shape[1]:
                bool_vector = np.argwhere(mask[:, i])
                blob_line.append(np.median(bool_vector))
            line_dict[str(blob)] = np.sum(np.abs(np.diff(blob_line)))

        longest_shifting_line = np.argmax(list(line_dict.values()))
        return

    def vitreos_blob(self, map):
        # extract blobs from clusters
        label_img = label(map)
        regions = regionprops(label_img)

        # filter for small blobs
        regions = [region for region in regions if region.bbox_area > 1000]

        blob_width_span = [region.bbox[3] - region.bbox[1] for region in regions]
        top_three_wide = list(np.sort(blob_width_span)[-3:])

        # remove any element if smaller by 100 pixels from w.r.t. widest
        max_span = np.max(top_three_wide)
        temp_list = []
        for i in range(len(top_three_wide)):
            abs_diff = np.abs(top_three_wide[i] - max_span)
            if abs_diff < 100:
                temp_list.append(top_three_wide[i])
                # top_three_wide = list(np.sort(top_three_wide))
            else:
                continue

        # assign new list
        top_three_wide = temp_list

        # check which of top blobs have highest located pixel
        blob_log = {}
        for span in top_three_wide:
            for region in regions:
                xspan = region.bbox[3] - region.bbox[1]
                if xspan == span:
                    blob_log[str(region.label)] = region.bbox[0]

        if len(top_three_wide) == 3:
            # get top located element
            middle_values = np.median(list(blob_log.values())).astype(int)
            middle_element = list(blob_log.values()).index(middle_values)
            vitreous_key = list(blob_log.keys())[middle_element]
        else:
            # get top located element
            top_height_element = np.argmin(list(blob_log.values()))
            vitreous_key = list(blob_log.keys())[top_height_element]

        # get the vitreous boolean map
        vitreous_map = label_img == int(vitreous_key)
        return vitreous_map

    def set_top_layer(self,layer, map):
        for k, num in enumerate(layer):
            if np.isnan(num):
                continue
            else:
                map[int(num), k] = 1
        return map

    def merge_close_blobs(self, map):
        for i in range(map.shape[0]):
            map_row = map[i, :]
            for row_idx in range(map_row):
                continue
        pass

    def get_layer_start_end(self, layer):
        for i, tl in enumerate(layer):
            if not np.isnan(tl):
                top_layer_xmin = i
                break
        for i, tl in enumerate(reversed(layer)):
            if not np.isnan(tl):
                top_layer_xmax = len(layer) - i
                break
        return top_layer_xmin, top_layer_xmax

    def fill_layer_sides(self, layer, min, max):
        for i, tl in enumerate(layer):
            if not np.isnan(tl):
                top_layer_min_value = tl
                stop_left = i
                break
        for i, tl in enumerate(reversed(layer)):
            if not np.isnan(tl):
                top_layer_max_value = tl
                stop_right = len(layer) - i
                break

        for k, layer_value in enumerate(layer[min:]):
            if np.isnan(layer_value):
                layer[k] = top_layer_min_value
            if k == stop_left:
                break

        for k, layer_value in enumerate(reversed(layer[:max])):
            if np.isnan(layer_value):
                layer[len(layer[:max - 1]) - k] = top_layer_max_value
            if (len(layer) - k) == stop_right:
                break
        return layer

    def plot_contour(self, x, peaks, contour_heights):
        plt.plot(x)
        plt.plot(peaks, x[peaks], "x")
        plt.vlines(x = peaks, ymin = contour_heights, ymax = x[peaks])
        plt.show()

    def find_peaks(self, image):
        first_peak_line = []

        for i in range(image.shape[1]):
            line = image[:, i]
            peaks, properties = find_peaks(line)
            prominences = peak_prominences(line, peaks)[0]
            contour_heights = line[peaks] - prominences

            two_peaks = np.sort(line[peaks])[-2:]
            peak_loc = []
            for peak in two_peaks:
                bool_peak = line[peaks] == peak
                arg_where = peaks[bool_peak]
                peak_loc.append(arg_where[0])

            first_peak = np.min(peak_loc)
            first_peak_line.append(first_peak)
        for k, fp in enumerate(first_peak_line):
            image[0:fp, k] = 0
        return image

    def correct_brightness(self, img, gamma):
        lookUpTable = np.empty((1, 256), np.uint8)
        for i in range(256):
            lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
        res = cv2.LUT(img, lookUpTable)
        return res

    def estimate_noise(self, I):
        I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)

        H, W = I.shape

        M = [[1, -2, 1],
             [-2, 4, -2],
             [1, -2, 1]]

        sigma = np.sum(np.sum(np.absolute(convolve2d(I, M))))
        sigma = sigma * np.math.sqrt(0.5 * np.math.pi) / (6 * (W - 2) * (H - 2))
        return sigma

    def preprocess_image(self):
        if np.mean(self.image) > 200:
            self.image = np.invert(self.image)

        # filter and smooth the image
        filtered_image, grey = self.filter_image(self.image)

        # threshold top layer
        roi_img, rpe_roi, thresh_map = self.filtered_image_threshold(filtered_image)

        # filter out small blobs focusing on smaller pixel clusters
        filtered_thresh_map = self.keep_largest_blob(thresh_map)

        # calculate gradient map for thresholding
        gradient_map, sobely = self.gradient_threshold(filtered_image)
        # sobely[rpe_roi==0] = 0
        # gradient_map, sobely = self.negative_gradient_threshold(sobely)

        # closed_gradient_map = self.closing(gradient_map)

        # cv2.imwrite(os.path.join("./test", self.image_name), posterior_hylaoid_mem_removal)
        vitreous_blob = self.vitreos_blob(gradient_map)
        vitreous_layer = self.get_top_layer(filtered_thresh_map, pad=False)

        gradient_map = self.remove_blobs_above_vitreous(gradient_map, vitreous_layer)

        # assign top layer for blob consistency
        gradient_map = self.set_top_layer(vitreous_layer, gradient_map)

        # gradient_map[extended_mask == 1] = 0
        closed_gradient_map = self.closing(gradient_map)

        # filter out small blobs focusing on smaller pixel clusters
        filtered_gradient_map = self.blob_filtering(closed_gradient_map, filter_size = 1000)

        # get top layer of retina
        top_layer = self.get_top_layer(filtered_gradient_map, pad=True)

        # interpolate any missing in btw region of top layer
        top_layer = self.interpolate_missing_regions(top_layer, kind="linear")

        tl_xmin, tl_xmax = self.get_layer_start_end(top_layer)

        # extract sobel roi of interest
        gradient_map_roi, shift_log = self.closed_gradient_map_lower_roi(filtered_gradient_map, top_layer)

        # in case no bottom layer can be detection, roi is just zero and bottom layer has to be imputed
        if np.unique(gradient_map_roi).shape[0] == 1:
            print("no bottom layer detected")
            closed_gradient_map = gradient_map_roi
        else:
            # threshold sobely roi
            threshold_value = filters.threshold_otsu(gradient_map_roi)
            gradient_map_roi = gradient_map_roi > threshold_value

            # filter out small objects
            closed_gradient_map = self.final_two_blob_filtering(gradient_map_roi)
        bottom_layer_map = self.set_bottom_layer(closed_gradient_map)
        bottom_layer = self.get_bottom_layer(bottom_layer_map, shift_log)

        if np.sum(np.isnan(bottom_layer)) == len(bottom_layer):
            # if not bottom layer was possible to derive, set it manually
            bottom_layer = np.linspace(top_layer[0] + 80, top_layer[0] + 80, top_layer.shape[0],
                                       endpoint=True).astype("int")
        else:
            bottom_layer = self.fill_layer_sides(bottom_layer, tl_xmin, tl_xmax)

        final_map = np.zeros(self.image.shape)

        # set top layer
        for k, num in enumerate(top_layer):
            if np.isnan(num):
                continue
            else:
                final_map[num, k] = 1

        # set bottom layer
        for k, num in enumerate(bottom_layer):
            if np.isnan(num):
                continue
            else:
                final_map[int(num), k] = 1

        # fill btw final two layers
        for col in np.arange(0, final_map.shape[1]):
            layer_bool = np.where(final_map[:, col] == 1)[0]
            if layer_bool.size == 0:
                layer_bool = 0

            min_row = np.min(layer_bool)
            max_row = np.max(layer_bool)
            final_map[min_row:max_row, col] = 1
        self.display_result([self.image, final_map], "./test_figures/{}".format(self.image_name))
        return self.processed_image

    '''
          # center index of distribution
          threshold_value = filters.threshold_otsu(sobely)

          # create copy for rpe calculation
          blur_copy = np.copy(blur)

          for col in np.arange(0, gray.shape[1]):
              if np.max(blur_copy[:, col]) < threshold_value:
                  threshold_value_temp = np.max(blur_copy[:,col]) / 3
              else:
                  threshold_value_temp = threshold_value
              labeled_foreground = (blur_copy[:, col] > threshold_value_temp)
              intensity_row_center = int(np.nanmean(np.argwhere(labeled_foreground)))
              # set above columns intensity distribution to zero
              blur_copy[0:intensity_row_center, :] = 0

          # brightest pixel in each column is RPE
          id_max = np.argmax(blur_copy, 0)

          # remove outlier RPE pixels
          rpe_locations = []
          for i in range(id_max.shape[0]):
              # keep iter below 511
              iter_ = i % 511
              examine_pix = id_max[iter_]
              prev_pix = id_max[iter_ - 1]
              next_pix = id_max[iter_ + 1]

              # get difference in location
              prev_diff = np.abs(examine_pix - prev_pix)
              next_diff = np.abs(examine_pix - next_pix)

              # throw away any rpe pixels with larger differences than 50
              if np.mean(prev_diff) > 50 or np.mean(next_diff) > 50:
                  rpe_locations.append(np.nan)
              else:
                  rpe_locations.append(examine_pix)

          # remove RPE pixels in column with low signal/noise ratio
          column_s2n_blur = self.signaltonoise(blur, axis = 0)

          # get significant low thresh
          sign_s2n = np.quantile(column_s2n_blur, 0.25)

          # find all column with sign low s2n values
          s2n_filter = column_s2n_blur < sign_s2n

          # set where there is low s2n values to nan
          rpe_locations = np.array(rpe_locations)
          rpe_locations[s2n_filter] = np.nan

          # fit second order polynomial through RPE points
          nan_bool = np.isnan(rpe_locations)
          x = np.arange(rpe_locations.shape[0])

          x_fit = x[~nan_bool]
          y_fit = rpe_locations[~nan_bool]

          f = interpolate.interp1d(x_fit, y_fit, kind=2)

          # interpolate
          x_new = np.arange(np.min(x_fit), np.max(x_fit), 1)
          y_new = f(x_new)

          rpe_center = np.zeros(512)
          rpe_center[0:y_new.shape[0]] = y_new
          rpe_center[y_new.shape[0]:] = y_new[-1]
          rpe_center = rpe_center.astype(int)

          test = np.copy(gray)
          for i, rpe_c in enumerate(rpe_center):
              print(rpe_c)
              test[rpe_c, i] = 255

          '''
